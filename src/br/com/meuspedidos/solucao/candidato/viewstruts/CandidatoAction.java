package br.com.meuspedidos.solucao.candidato.viewstruts;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import br.com.meuspedidos.solucao.candidato.controller.CandidatoFacade;
import br.com.meuspedidos.solucao.exceptions.ApplicationException;
import br.com.meuspedidos.solucao.utilidades.Messages;

public final class CandidatoAction extends DispatchAction{
	public ActionForward abrirCadastro( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) {
		/** fazendo o casting do form */
		CandidatoForm meuForm = (CandidatoForm) form;
		meuForm.limparCampos();

		return mapping.findForward( "candidato_campos" );
	}

	public ActionForward enviar( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) {

		/** fazendo o casting do form */
		CandidatoForm meuForm = (CandidatoForm) form;
		try {
			CandidatoFacade.getInstance().enviar( meuForm.montarPO() );
			meuForm.limparCampos();
			this.addMessages( request, Messages.createMessages( "sucesso" ) );

		} catch ( ApplicationException e ) {
			this.addMessages( request, Messages.createMessages( "falha", new String[ ] { e.getMessage() } ) );
			e.printStackTrace();
		} catch ( ParseException e ) {
			this.addMessages( request, Messages.createMessages( "falha", new String[ ] { "Erro ao converter dados do campos" } ) );
			e.printStackTrace();
		} catch ( Exception e ) {
			e.printStackTrace();
			this.addMessages( request, Messages.createMessages( "erro.desconhecido", new String[ ] { e.getMessage() } ) );

		}

		return mapping.findForward( "candidato_campos" );
	}

	public ActionForward limpar( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) {
		/** fazendo o casting do form */
		CandidatoForm meuForm = (CandidatoForm) form;

		meuForm.limparCampos();

		return mapping.findForward( "candidato_campos" );
	}

}
