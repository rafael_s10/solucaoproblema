package br.com.meuspedidos.solucao.candidato.viewstruts;

import java.text.ParseException;
import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import br.com.meuspedidos.solucao.candidato.model.CandidatoPO;
import br.com.meuspedidos.solucao.utilidades.Utilidades;

/**
 * Classe respons�vel por conter um atributo para cada campo na tela(JSP). Todas
 * as altera��es que quiser fazer nos campos da tela, dever�o ser feitas no
 * FORM. Isso porque a tela � carregada com base numa classe FORM, sendo assim,
 * tudo que fizer na classe FORM ser� refletido nos campos da tela.
 * 
 * 
 * 
 * 
 * @author Rafael Guimaraes Santos <rafael_s10@live.com>
 * @since 23/01/2015 09:08:47
 * @version 1.0
 */

public final class CandidatoForm extends ActionForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2798364118261230978L;
	/** Declarando os atributos que representarao os campos da tela (jsp) */

	private String codigo;
	private String nome;
	private String email;
	private String html;
	private String css;
	private String javascript;
	private String python;
	private String django;
	private String desenvolvimentoIos;
	private String desenvolvimentoAndroid;

	ArrayList< CandidatoPO > listaCandidato = new ArrayList< CandidatoPO >();

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome( String nome ) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail( String email ) {
		this.email = email;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml( String html ) {
		this.html = html;
	}

	public String getCss() {
		return css;
	}

	public void setCss( String css ) {
		this.css = css;
	}

	public String getJavascript() {
		return javascript;
	}

	public void setJavascript( String javascript ) {
		this.javascript = javascript;
	}

	public String getPython() {
		return python;
	}

	public void setPython( String python ) {
		this.python = python;
	}

	public String getDjango() {
		return django;
	}

	public void setDjango( String django ) {
		this.django = django;
	}

	public String getDesenvolvimentoIos() {
		return desenvolvimentoIos;
	}

	public void setDesenvolvimentoIos( String desenvolvimentoIos ) {
		this.desenvolvimentoIos = desenvolvimentoIos;
	}

	public String getDesenvolvimentoAndroid() {
		return desenvolvimentoAndroid;
	}

	public void setDesenvolvimentoAndroid( String desenvolvimentoAndroid ) {
		this.desenvolvimentoAndroid = desenvolvimentoAndroid;
	}

	public ArrayList< CandidatoPO > getListaCandidato() {
		return listaCandidato;
	}

	public void setListaCandidato( ArrayList< CandidatoPO > listaCandidato ) {
		this.listaCandidato = listaCandidato;
	}

	public void preencherCandidato( CandidatoPO poEncontrado ) throws ParseException, java.text.ParseException {
		this.setCodigo( poEncontrado.getCodigo().toString() );
		this.setNome( poEncontrado.getNome() );
		this.setEmail( poEncontrado.getEmail() );
		this.setCss( poEncontrado.getCss().toString() );
		this.setDesenvolvimentoAndroid( poEncontrado.getDesenvolvimentoAndroid().toString() );
		this.setDesenvolvimentoIos( poEncontrado.getDesenvolvimentoIos().toString() );
		this.setDjango( poEncontrado.getDjango().toString() );
		this.setHtml( poEncontrado.getHtml().toString() );
		this.setJavascript( poEncontrado.getJavascript().toString() );
		this.setPython( poEncontrado.getPython().toString() );
	}

	public CandidatoPO montarPO() throws ParseException, java.text.ParseException {
		CandidatoPO po = new CandidatoPO();

		if ( !Utilidades.isNuloOuVazio( this.getCodigo() ) ) {
			po.setCodigo( Long.valueOf( this.getCodigo() ) );

		}

		if ( !Utilidades.isNuloOuVazio( this.getCss() ) ) {
			po.setCss( Byte.valueOf( this.getCss() ) );

		}

		if ( !Utilidades.isNuloOuVazio( this.getDesenvolvimentoAndroid() ) ) {
			po.setDesenvolvimentoAndroid( Byte.valueOf( this.getDesenvolvimentoAndroid() ) );

		}

		if ( !Utilidades.isNuloOuVazio( this.getDesenvolvimentoIos() ) ) {
			po.setDesenvolvimentoIos( Byte.valueOf( this.getDesenvolvimentoIos() ) );

		}

		if ( !Utilidades.isNuloOuVazio( this.getDjango() ) ) {
			po.setDjango( Byte.valueOf( this.getDjango() ) );

		}

		if ( !Utilidades.isNuloOuVazio( this.getHtml() ) ) {
			po.setHtml( Byte.valueOf( this.getHtml() ) );

		}

		if ( !Utilidades.isNuloOuVazio( this.getJavascript() ) ) {
			po.setJavascript( Byte.valueOf( this.getJavascript() ) );

		}

		if ( !Utilidades.isNuloOuVazio( this.getPython() ) ) {
			po.setPython( Byte.valueOf( this.getPython() ) );

		}

		po.setNome( this.getNome() );
		po.setEmail( this.getEmail() );
		return po;
	}

	public void limparCampos() {
		setCodigo( "" );
		setNome( "" );
		setEmail( "" );
		setCss( "" );
		setDesenvolvimentoAndroid( "" );
		setDesenvolvimentoIos( "" );
		setDjango( "" );
		setHtml( "" );
		setJavascript( "" );
		setPython( "" );

	}

}
