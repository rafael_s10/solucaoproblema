package br.com.meuspedidos.solucao.candidato.controller;

import java.util.ArrayList;

import br.com.meuspedidos.solucao.candidato.model.CandidatoPO;
import br.com.meuspedidos.solucao.candidato.model.CandidatoSERVICE;
import br.com.meuspedidos.solucao.exceptions.ApplicationException;
import br.com.meuspedidos.solucao.exceptions.InserirException;

public class CandidatoFacade{
	/**
	 * Atributo responsável por possibilitar o acesso da Camada Controller a
	 * Camada MODEL
	 */
	private final CandidatoSERVICE SERVICE;

	// ********************************************** \\
	// ************ APLICANDO SINGLETON ************* \\
	// ********************************************** \\
	private static CandidatoFacade instance;

	private CandidatoFacade(){
		SERVICE = new CandidatoSERVICE();
	}

	public static CandidatoFacade getInstance() {
		if ( instance == null ) {
			instance = new CandidatoFacade();
		}
		return instance;
	}

	// ********************* FIM ******************** \\
	// ************ APLICANDO SINGLETON ************* \\
	// ********************* FIM ******************** \\

	public void enviar( CandidatoPO po ) throws ApplicationException, InserirException {
		SERVICE.enviar( po );
	}

	
}
