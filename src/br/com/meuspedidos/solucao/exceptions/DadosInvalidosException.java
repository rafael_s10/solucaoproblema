package br.com.meuspedidos.solucao.exceptions;

/**
 * Classe que representa o erro ocorrido no ato da
 * confirma��o dos processos realizados na conex�o.
 * 
 * @author Rafael Guimaraes
 * @since 14/10/2015
 * @version 1.0
 */

public final class DadosInvalidosException extends Exception{
	public DadosInvalidosException( String message ){
		super( message );
	}

	public DadosInvalidosException( String message, Throwable cause ){
		super( message, cause );
	}
}
