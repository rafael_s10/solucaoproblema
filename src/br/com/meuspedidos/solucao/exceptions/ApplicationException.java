package br.com.meuspedidos.solucao.exceptions;

import java.sql.SQLException;

/**
 * Classe que representa o erro ocorrido na Aplica��o.
 * 
 * Este erro ser� lan�ado sempre para as camadas de front-end.
 * 
 * @author Rafael Guimar�es Santos
 * @since 14/10/2013
 * @version 1.0
 */
public final class ApplicationException extends SQLException{

	public ApplicationException( String message ){
		super( message );
	}

	public ApplicationException( String message, Throwable cause ){
		super( message, cause );
	}
}
