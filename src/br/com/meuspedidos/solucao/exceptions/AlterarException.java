package br.com.meuspedidos.solucao.exceptions;

import java.sql.SQLException;

public final class AlterarException extends SQLException{

	public AlterarException( String message ){
		super( message );
	}

	public AlterarException( String message, Throwable cause ){
		super( message, cause );
	}
}
