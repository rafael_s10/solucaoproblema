<!DOCTYPE html>
<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1"pageEncoding="ISO-8859-1"%> --%>
<!-- <!DOCTYPE html > -->
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!-- http://getbootstrap.com/
http://getbootstrap.com/components/
https://code.jquery.com/jquery-1.10.2.min.js -->

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Hello Web World</title>
		
		<script type="text/javascript">
		$(document).ready(function(){
			$('#form_candidato').attr("autocomplete", "off");
			
			$(".inteiro").maskMoney({
				   thousands : '',
				   decimal : '.',
				   allowZero : false,
				   precision : 0
			});
			
			/*definindo campos obrigatorios */
			$('#nome').attr("required", "required");
			
			$("#datepicker").datepicker({
		         dateFormat:'dd/mm/yy',
		         dayNames: ['Domingo','Segunda','Ter�a','Quarta','Quinta','Sexta','S�bado'],
		         dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		         dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S�b','Dom'],
		         monthNames: ['Janeiro','Fevereiro','Mar�o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		         monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		         nextText: 'Pr�ximo',
		         prevText: 'Anterior' 
		          });
		    
		    $("#datepicker").mask("99/99/9999"); 
		});
		
		function executar( nomeMetodo ){
		    document.getElementById("form_candidato").method.value = nomeMetodo;
			}
		
		function limpar( nomeMetodo ){
		    document.getElementById("form_candidato").method.value = nomeMetodo;
			}

			function checkRegexp(o, regexp, n) {
				if (!(regexp.test(o))) {
					return false;
				} else {
					return true;
				}
			}
		</script>
		
		<link href="assets/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet" media="screen">
	</head>
	<body>
		
		<html:form styleId="form_candidato" action="/candidatoAction.do" method="post">
		<html:hidden property="method" value="empty"/>
		
		<div class="container">
			
			<div class="panel panel-group panel-primary">
				
				<!-- Esse panel � o cabe�alho da p�gina -->
				<div class="panel-heading">
					<i class="glyphicon glyphicon-user"></i>
					Candidato
				</div>
				
				
				<!-- Esse � o painel do corpo da minha janela -->
				<div class="panel-body">
					
					<!-- Linha -->
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bg-danger">
							<form>
							
								<div class="row">
										
									<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<label for="nome" class="control-label" >Nome</label>
										<html:text  styleId="nome" styleClass="form-control obrigatorio"  property="nome" name = "candidatoForm"/>
									</div>
									
									<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<label for="email" class="control-label">E-mail</label>
										<html:text  styleId="email" styleClass="form-control obrigatorio"  property="email" name = "candidatoForm"/>
									</div>
										
									<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<label for="css" class="control-label" >Css</label>
										<html:text  styleId="css" styleClass="form-control "  property="css" name = "candidatoForm"/>
									</div>	
									
									<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<label for="javascript" class="control-label" >Javascript</label>
										<html:text  styleId="javascript" styleClass="form-control inteiro"  property="javascript" name = "candidatoForm"/>
									</div>	
									
									<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<label for="python" class="control-label" >Python</label>
										<html:text  styleId="python" styleClass="form-control "  property="python" name = "candidatoForm"/>
									</div>	
									
									<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<label for="django" class="control-label" >Django</label>
										<html:text  styleId="django" styleClass="form-control datepicker"  property="django" name = "candidatoForm"/>
									</div>
									
									<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<label for="desenvolvimentoIos" class="control-label" >Desenvolvimento IOS</label>
										<html:text  styleId="desenvolvimentoIos" styleClass="form-control"  property="desenvolvimentoIos" name = "candidatoForm"/>
									</div>	
									
									<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<label for="desenvolvimentoAndroid" class="control-label" >Desenvolvimento Android</label>
										<html:text  styleId="desenvolvimentoAndroid" styleClass="form-control"  property="desenvolvimentoAndroid" name = "candidatoForm"/>
									</div>	
									
									<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<label for="html" class="control-label" >HTML</label>
										<html:text  styleId="html" styleClass="form-control"  property="html" name = "candidatoForm"/>
									</div>				
								</div>
								
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										
										<button type="submit" id="enviar" class="btn btn-success">Enviar</button>										
										<button type="button" id="limpar" class="btn btn-warning">Limpar</button>
										
									</div>
								</div>
								</form>						
						</div>
					</div>
				</div>

				<!-- Esse panel � o Rodape da p�gina -->
				<div class="footer">
					Meus Pedidos &copy;
				</div>
			</div>
		
		</div>
		</html:form>
		
		<script src="assets/js/jquery-1.10.2.min.js" type="text/javascript"></script>
	
		<script type="text/javascript">
			$("#enviar").click(function(){
				executar( "enviar" );
			});
			
			$("#limpar").click(function(){
				limpar( "limpar" );
			});
		</script>
		
		<script type="text/javascript">
			
			
			$("#limpar").click(function(){
				limpar( "limpar" );
			});
		</script>
	</body>	
</html>